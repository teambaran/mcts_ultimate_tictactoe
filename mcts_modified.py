from mcts_node import MCTSNode
import random
import time
import copy
from random import choice
from math import sqrt, log


num_nodes = 1000
explore_faction = 1. #exploitation parameter
global sampled_game
global identity
global node_count
global count
def traverse_nodes(node, board, state, identity):
    global sampled_game
    """ Traverses the tree until the end criterion are met. Finding a leaf node

    Args:
        node:       A tree node from which the search is traversing.
        board:      The game setup.
        state:      The state of the game.
        identity:   The bot's identity, either 'red' or 'blue'.

    Returns:        A node from which the next stage of the search can proceed.

    """
    UCT = {}
    if len(node.untried_actions) > 0:
        
        return node
    else:
        for key in node.child_nodes:
            node.child_nodes[key].untried_actions = verify_untried(node.child_nodes[key], state, board)
            if len(node.child_nodes[key].untried_actions) > 0:
                current_node = node.child_nodes[key]
                node_uct = calculate_uct(current_node.wins, current_node.visits, node.visits)                
                UCT[current_node] = node_uct      
        if UCT:
            sampled_game = board.next_state(state, greatest_uct(UCT).parent_action)
            greatest_uct(UCT).untried_actions = verify_untried(greatest_uct(UCT), sampled_game, board)
            return greatest_uct(UCT)
        else:
            return traverse_nodes(greatest_win_value(node, node.child_nodes), board, state, identity)
                
  
    
def expand_leaf(node, board, state, identity):#adds child to node found in traverse_nodes
    global sampled_game
    """ Adds a new leaf to the tree by creating a new child node for the given node.

    Args:
        node:   The node for which a child will be added.
        board:  The game setup.
        state:  The state of the game.

    Returns:    The added child node.

    """
    old_state = copy.deepcopy(state)
    if not my_turn(state, board, identity):
    
        sampled_game = enemy_move(state, board, identity)
    random_move=node.untried_actions[0]
    for move in range(0,len(node.untried_actions)-1):
        random_move=node.untried_actions[move]
        teststate=board.next_state(state,random_move)
        if board.is_ended(teststate):
            break
    
    random_move_list = list(random_move)
    legal_acts = board.legal_actions(old_state)
    #print("in expand leaf")
    for item in legal_acts:
        item_list = list(item)
        if item_list == random_move_list:
            legal_acts.remove(item)
        
    sampled_game = board.next_state(sampled_game, random_move)
    sampled_game = enemy_move(sampled_game, board, identity)
    
    new_node = MCTSNode(parent=node, parent_action=random_move, action_list=board.legal_actions(sampled_game))#creating the new node to be added
    new_node.untried_actions = verify_untried(new_node, sampled_game, board)
    
    state = board.next_state(state, random_move)       
    new_node.visits = 1#it has been visited once during creation
    node.untried_actions.remove(random_move)
    node.child_nodes[tuple(random_move)] = new_node#adding new node to children of passed in node

    return new_node# , node.child_nodes, node.untried_actions

def rollout(board, state):#should not return anything. Its terminal state is used to update nodes of the tree
    """ Given the state of the game, the rollout plays out the remainder randomly.

    Args:
        board:  The game setup.
        state:  The state of the game.

    """
    while not board.is_ended(state):
        legal_acts = board.legal_actions(state)
        random_move = choice(legal_acts)
        state = board.next_state(state, random_move)

    return state
    
        
def backpropagate(node, won):
    """ Navigates the tree from a leaf node to the root, updating the win and visit count of each node along the path.

    Args:
        node:   A leaf node.
        won:    An indicator of whether the bot won or lost the game.

    """
    if won == identity:
        if node.parent == None:
            node.wins +=1
            node.visits += 1
        i = 0
        while node is not None:
            if i%2 == 0:
                node.wins += 1
                node.visits += 1
                node = node.parent
            else:
                node=node.parent
            i+=1
    if not won == identity:
        if node.parent == None:
            node.visits += 1
        i = 0
        while node is not None:
            if i%2 == 0:
                node.visits += 1
                node = node.parent
            else:
                node=node.parent
            i+=1
    pass

def think(board, state):#returns the next 'ideal' move found via MCTS
    global sampled_game
    global identity
    global root_node
    global node_count
    global root_node
    """ Performs MCTS by sampling games and calling the appropriate functions to construct the game tree.
    Args:
        board:  The game setup.
        state:  The state of the game.
    Returns:    The action to be taken.
    """

    identity_of_bot = board.current_player(state)
    identity = 'red' if identity_of_bot == 1 else 'blue'
    root_node = MCTSNode(parent=None, parent_action=None, action_list=board.legal_actions(state))
    # Start at root
    root_node.visits = 1

    for step in range(num_nodes):

        # Copy the game for sampling a playthrough
        sampled_game = state 
        # Start at root
        node = root_node

        leaf = traverse_nodes(node, board, sampled_game, identity)
            
        if not board.is_ended(sampled_game):
            new_leaf = expand_leaf(leaf, board, sampled_game, identity)
            sampled_game = rollout(board, sampled_game)
        
        
        
        win_vals = board.win_values(sampled_game)
        win = None
        if identity == 'red' and win_vals[1] == 1:
            win = 'red'
        elif identity == 'blue' and win_vals[2] == 1:
            win = 'blue'
        backpropagate(leaf, win)

    children = root_node.child_nodes

    max_win = -1.0
    best_leaf = None
    for child in children:
        temp = children[child].wins/children[child].visits
        if temp >= max_win:
            max_win = temp
            best_leaf = children[child]
        # Do MCTS - This is all you!
    # Return an action, typically the most frequently used action (from the root) or the action with the best
    # estimated win rate.
    #time.sleep(1) #for debug
    return best_leaf.parent_action
    
####################################################################
def greatest_win_value(node, dictionary):
    if not dictionary:
        return node
    greatest_win_node = None
    win_val = -99999
    for key in dictionary:
        if dictionary[key].wins > win_val:
            win_val = dictionary[key].wins
            greatest_win_node = dictionary[key]
    return greatest_win_node
            
def greatest_uct(dictionary):
    max_uct = -99999
    node = None
    for key in dictionary:
        if dictionary[key] >= max_uct:
            max_uct = dictionary[key]
            node = key
    return key
def smallest_uct(dictionary):
    mix_uct = 99999
    node = None
    for key in dictionary:
        if dictionary[key] <= max_uct:
            min_uct = dictionary[key]
            node = key
    return key
        
def calculate_uct(wins_after, sims_after, parent_sims):
    return (wins_after/sims_after) + (explore_faction * sqrt(log(parent_sims)/sims_after))

def my_turn(state, board, identity):
    if identity == 'red' and board.current_player(state) == 1:
        return True
    else:
        return False
def enemy_move(state, board, identity):
    if not my_turn(state,board, identity):
        possible_moves = board.legal_actions(state)
        if len(possible_moves) > 0:
            random_move = choice(possible_moves)
            state = board.next_state(state, random_move)

        return state
def verify_untried(node, state, board):
    untried = node.untried_actions
    
    children = node.child_nodes
    legal = board.legal_actions(state)
    legal_list = []
    untried_list = []
    for move in untried:
        untried_list.append(list(move))
    for move in legal:
        legal_list.append((list(move)))
    child_list = []
    for action in children:
        child_list.append(list(action))
    for move in legal_list:
        if move not in child_list and move not in untried_list:
            untried_list.append(tuple(move))
    return untried_list